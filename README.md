# EmberIgnite Hopin registrer

Recorded puppeteer script from hopin registration.

## Installation

```sh
yarn
```

## How to use

I don't have stable enough internet to finish the script.
Or hopin network is *really* slow.
Either or, it's about making the calls parametrised.
If you look into `chrome-recording.js`,
you will find references to **michal.bryxi**
or **John** or **Doe**.

Make those parametrised and call the script in loops.

I'm not sure how stable respective selectors will be though,
but let's hope.

```sh
yarn start
```

Now you should get a Chromium incognito window
that will try to fill in the hopin registration.
